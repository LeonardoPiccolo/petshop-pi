/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.servlet;

import com.mycompany.petshoppi.Entidade.Clientes;
import com.mycompany.petshoppi.dao.ClienteDAO;
import com.mycompany.petshoppi.util.Utils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//NÃO MECHER ESSE É O BASE !! //
/**
 *
 * @author Sthefany
 */
public class AlterarCliente extends HttpServlet {

   
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
        String cpf = request.getParameter("cpf");
        Clientes cliente = ClienteDAO.consultarCliente(cpf);
        
        request.setAttribute("cliente", cliente);
        
         request.getRequestDispatcher("/Cliente/CadCliente.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         String sexo ="";
         String teste = request.getParameter("cod_cliente");
         String cpf = request.getParameter("CPF");
         String nome = request.getParameter("Nome");
         String email = request.getParameter("Email");
         int cod_Cliente =  Integer.parseInt(teste);
         String dataNascimento = request.getParameter("DataNascimento");
         if(request.getParameter("Feminino") != null){
           sexo = "F";
         }else if(request.getParameter("Masculino") != null){
           sexo = "M";
         }else{
           sexo = " ";
         }
         String contato = request.getParameter("Contato");
         String status_Clientes = request.getParameter("status_Clientes");
         String CEP = request.getParameter("CEP");
         String NumeroEnd = request.getParameter("NumeroEnd");
         String ComplementoEnd = request.getParameter("ComplementoEnd");
         
         
         Clientes cliente = new  Clientes( cod_Cliente,  nome,  cpf,  dataNascimento,  sexo,  email,
             contato,  status_Clientes,  CEP,NumeroEnd ,ComplementoEnd);
         boolean ok = ClienteDAO.alterarCliente(cliente);
         Utils.RedirecionarURL(ok, response);
    }

   
}
