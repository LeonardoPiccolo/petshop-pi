package com.mycompany.petshoppi.servlet;

import com.mycompany.petshoppi.Entidade.Clientes;
import com.mycompany.petshoppi.dao.ClienteDAO;
import com.mycompany.petshoppi.util.Utils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//NÃO MECHER ESSE É O BASE !! //
public class ClientesServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

        List<Clientes> ListClientes = ClienteDAO.listarClientes();
        request.setAttribute("ListaClientes", ListClientes);
        request.getRequestDispatcher("Cliente/Index.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String sexo = "";
        String cpf = request.getParameter("cpf");
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        int cod_Cliente = 0;
        String dataNascimento = request.getParameter("DataNascimento");
        if (request.getParameter("Feminino") != null) {
            sexo = "F";
        } else if (request.getParameter("Masculino") != null) {
            sexo = "M";
        } else {
            sexo = "";
        }
        String contato = request.getParameter("email");
        String status_Clientes = request.getParameter("email");
        String CEP = request.getParameter("email");
        String NumeroEnd = request.getParameter("email");
        String ComplementoEnd = request.getParameter("email");

        Clientes cliente = new Clientes(cod_Cliente, nome, cpf, dataNascimento, sexo, email,
                contato, status_Clientes, CEP, NumeroEnd, ComplementoEnd);
        boolean ok = ClienteDAO.cadastrarCliente(cliente);
        Utils.RedirecionarURL(ok, response);
       
    }
}
