
package com.mycompany.petshoppi.util;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao 
{
    public static String status = "Não conectado";
   // public static String driver = "org.apache.derby.jdbc.EmbeddedDriver";   //DRIVER DO MYSQL A PARTIR DA VERSÃO 8                   
    public static String driver = "com.mysql.cj.jdbc.Driver";   //DRIVER DO MYSQL A PARTIR DA VERSÃO 8                   
    //public static String server = "localhost";                  //ENDEREÇO DO SERVIDOR DE BANCO DE DADOS
    public static String server = "petshop.cdkummnyym4b.sa-east-1.rds.amazonaws.com"; //STRING DE CONEXÃO COM A AMAZON
    public static String database = "BD_PetShop";               //NOME DO BANCO DE DADOS
    //public static String login = "root";                        //USUÁRIO DO BANCO DE DADOS    
    public static String login = "admin";                        //USUÁRIO DO BANCO DE DADOS   
    //public static String senha = "";                       //SENHA DE ACESSO
    public static String senha = "animefany15";                       //SENHA DE ACESSO
   // public static String url = "jdbc:derby://localhost:1527/BD_PetShop [ on APP]";
    public static String url = "jdbc:mysql://petshop.cdkummnyym4b.sa-east-1.rds.amazonaws.com:3306/BD_PetShop [ on APP]"; //UPDATE PARA ESSA STRING DE CONEXÃO

    public static java.sql.Connection CONEXAO;

    public static java.sql.Connection getCONEXAO() throws SQLException {
        
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(Conexao.class.getName()).log.(Level.SEVERE,null,ex);
        }
        return DriverManager.getConnection(url, login, senha);
    }

//    public static void setCONEXAO(java.sql.Conexao CONEXAO) {
//        Conexao.CONEXAO = CONEXAO;
//    }

    
    public static java.sql.Connection abrirConexao() throws ClassNotFoundException, SQLException 
    {

        url = "jdbc:mysql://" + server + ":3306/" + database + "?useTimezone=true&serverTimezone=UTC&useSSL=false";

        if (CONEXAO == null) 
        {
            try 
            {
                Class.forName(driver);
                CONEXAO = DriverManager.getConnection(url, login, senha);
                
                if (CONEXAO != null) 
                {
                    status = "Conexão realizada com sucesso!";
                } 
                else 
                {
                    status = "Não foi possivel realizar a conexão";
                }
            } 
            catch (ClassNotFoundException e) 
            {  
              throw new ClassNotFoundException("O driver expecificado nao foi encontrado.");
            } 
            catch (SQLException e) 
            {  
              throw new SQLException("Erro ao estabelecer a conexão (Ex: login ou senha errados).");
            }
        } 
        else 
        {
            try 
            {
                if (CONEXAO.isClosed()) 
                {
                  CONEXAO = DriverManager.getConnection(url, login, senha);
                }
            } 
            catch (SQLException ex) 
            {
              throw new SQLException("Falha ao fechar a conexão.");
            }
        }
        return CONEXAO;
    }
    
     /**
     * Método responsável por Fechar a conexão com o Banco de Dados.
     * 
     * @return boolean informando se a conexão está fechada ou aberta
     * @throws SQLException 
     */
    public static boolean fecharConexao() throws SQLException 
    {
        boolean response = false;
        try 
        {
            if (CONEXAO != null) {
                if (!CONEXAO.isClosed()) {
                    CONEXAO.close();
                }
            }
            status = "Não conectado";
            response = true;
        } 
        catch (SQLException e) 
        {
            response = false;
        }
        return response;
    }
    public static String getStatusConexao() 
    {
        System.out.println("Status: " + status);
        return status;
    }
}
