
package com.mycompany.petshoppi.dao;
import com.mycompany.petshoppi.Entidade.Clientes;
import com.mycompany.petshoppi.bd.Config;
import com.mycompany.petshoppi.util.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sthefany
 */
//NÃO MECHER ESSE É O BASE !! //
public class ClienteDAO {
    
    static Connection conexao;

    /**
     * Driver do MySQL a partir da versão 8.0
     */
   // private static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    
    public ClienteDAO() {
    }
    
    public static boolean cadastrarCliente(Clientes cliente) {
               
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();
           
            String sql = "insert into Clientes ( nome, cpf, dataNascimento, sexo, email, contato,Status_Clientes, cep) "
                    + "values (?,?,?,?,?,?,?,?)";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setInt(1, cliente.getCod_Cliente());
            instrucaoSQL.setString(2, cliente.getNome());
            instrucaoSQL.setString(3, cliente.getCpf());
            instrucaoSQL.setString(4, cliente.getDataNascimento());
            instrucaoSQL.setString(5, cliente.getSexo());
            instrucaoSQL.setString(6, cliente.getEmail());
            instrucaoSQL.setString(7, cliente.getContato());
            instrucaoSQL.setString(8, cliente.getStatus_Clientes());
            instrucaoSQL.setString(9, cliente.getCEP());
            instrucaoSQL.setString(9, cliente.getComplementoEnd());
            instrucaoSQL.setString(9, cliente.getNumeroEnd());
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;
                
            } else {
                throw new Exception();

            }

                        
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean alterarCliente(Clientes cliente) {
        
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "UPDATE Cliente SET nome = ?, cpf = ?, dataNascimento = ?, sexo= ?, email = ?, contato = ?, status_Clientes = ?, CEP = ?, ComplementoEnd = ?, NumeroEnd = ? WHERE cod_Cliente = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cliente.getNome());
            instrucaoSQL.setString(2, cliente.getCpf());
            instrucaoSQL.setString(3, cliente.getDataNascimento());
            instrucaoSQL.setString(4, cliente.getSexo());
            instrucaoSQL.setString(5, cliente.getEmail());
            instrucaoSQL.setString(6, cliente.getContato());
            instrucaoSQL.setString(7, cliente.getStatus_Clientes());
            instrucaoSQL.setString(8, cliente.getCEP());
            instrucaoSQL.setString(9, cliente.getComplementoEnd());
            instrucaoSQL.setString(10, cliente.getNumeroEnd());
            instrucaoSQL.setInt(11, cliente.getCod_Cliente());
            
                    
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

            conexao.close();
            
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean removerCliente(String cliente) {
        
        boolean status = false;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "delete from Cliente where cpf = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cliente);
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }
            
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
            
        }
        
        return status;
        
    }
    
    public static boolean validarCliente(Clientes cliente) {
        
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try {
            
            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select * from Cliente where cpf = ?";

            instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cliente.getCpf());
            
            rs = instrucaoSQL.executeQuery();
    
            if (rs != null) {
                rs.close();
                System.out.println("Cliente existe!");

            }

            if (instrucaoSQL != null) {
                instrucaoSQL.close();
                System.out.println("Cliente não existe.");
            }
            
            conexao.close();
            
            
        } catch (Exception e) {
            System.out.println("Erro na consulta.");
            
        }
        
        return status;
        
    }
    
    public static Clientes consultarCliente(String cliente) {
        
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        Clientes cli = new Clientes();
        try {
            
           // Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select * from Cliente where cpf = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setString(1, cliente);
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) {
                
                cli.setCod_Cliente(rs.getInt("cod_Cliente"));
                cli.setNome(rs.getString("nome"));
                cli.setCpf(rs.getString("cpf"));
                cli.setDataNascimento(rs.getString("dataNascimento"));
                cli.setSexo(rs.getString("sexo"));
                cli.setEmail(rs.getString("email"));
                cli.setContato(rs.getString("contato"));
                cli.setStatus_Clientes(rs.getString("status_Clientes"));
                cli.setCEP(rs.getString("CEP"));
                cli.setComplementoEnd(rs.getString("ComplementoEnd"));
                cli.setNumeroEnd(rs.getString("NumeroEnd"));
            }
            
        } catch (Exception e) {
            System.out.println("Erro na consulta");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();
                    System.out.println("Cliente existe! Fim");

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                    System.out.println("Cliente não existe. Fim");

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return cli;
        
    }    
    
    public static ArrayList<Clientes> listarClientes() {
        
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<Clientes> listaCliente = new ArrayList<Clientes>();
        
        try {
            
           // Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select * from Cliente";

            instrucaoSQL = conexao.prepareStatement(sql);
            
            rs = instrucaoSQL.executeQuery();
            
            while (rs.next()) {
                
                Clientes cli = new Clientes();
                
                cli.setCod_Cliente(rs.getInt("cod_Cliente"));
                cli.setNome(rs.getString("nome"));
                cli.setCpf(rs.getString("cpf"));
                cli.setDataNascimento(rs.getString("dataNascimento"));
                cli.setSexo(rs.getString("sexo"));
                cli.setEmail(rs.getString("email"));
                cli.setContato(rs.getString("contato"));
                cli.setStatus_Clientes(rs.getString("status_Clientes"));
                cli.setCEP(rs.getString("CEP"));
                cli.setComplementoEnd(rs.getString("ComplementoEnd"));
                cli.setNumeroEnd(rs.getString("NumeroEnd"));
                listaCliente.add(cli);
                
            }
            
        } catch (Exception e) {
            System.out.println("Erro na listagem");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return listaCliente;
        
    }
    
    public static int pegarId(Clientes cliente) {
        
        int cod_Cliente = 0;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try {
            
            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select cod_Cliente from Clientes where cpf = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, cliente.getCpf());
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) {
                
                cod_Cliente = rs.getInt("cod_Cliente");
                
            }
            
        } catch (Exception e) {
            System.out.println("Erro na consulta");
            
        } finally {
            
            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }
            
        }
        
        return cod_Cliente;
        
    }
    
    
} 
