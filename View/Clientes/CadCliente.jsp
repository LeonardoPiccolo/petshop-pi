<!--NÃO MECHER ESSE É O BASE !! -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:import url="/Utils/Header.jsp"></c:import>      
    <body>
        <div class="body">
        <c:import url="/Utils/Menu.jsp"></c:import> 
        
            <div class="container">
                <h1>Cliente</h1>
                <c:if test="${not empty cliente}">
                    <form action="AlterarCliente" id="FormAlte" method="POST" class="row">
                        <input type="text" class="d-none" id="cod_cliente" name="cod_cliente" value="${cliente.cod_Cliente}">
                        <div class="mb-3 col-lg-4">
                            <label for="Nome" class="form-label">Nome*:</label>
                            <input type="text" value="${cliente.nome}" required class="form-control" name="Nome">
                        </div>
                        <div class="mb-3 col-lg-3">
                            <label for="CPF" class="form-label">CPF*:</label>
                            <input type="text" value="${cliente.cpf}" required class="form-control" name="CPF" id="CPF">
                        </div>
                        <div class="col-lg-4 align-items-center justify-content-center row">
                            <div class="form-check form-check-inline col-lg-4">                       
                                <input type="checkbox" ${(cliente.sexo == "f") ? "checked" : ""} class="form-check-input" name="Feminino" id="Feminino">
                                <label class="form-check-label" for="exampleCheck1">Feminino</label>
                            </div>
                            <div class="form-check form-check-inline col-lg-4">
                                <input type="checkbox" ${(cliente.sexo == "m") ? "checked" : ""} class="form-check-input" name="Masculino" id="Masculino">
                                <label class="form-check-label" for="exampleCheck1">Masculino</label>                
                            </div>  
                        </div>
                        <div class="mb-3 col-lg-2">
                            <label for="Status" class="form-label">Status</label>
                            <select class="form-select"  name="status_Clientes">
                                <option value="Ativo" ${(cliente.status_Clientes == "Ativo") ? "selected" : ""} ;>Ativo</option>
                                <option value="Premiun" ${(cliente.status_Clientes == "Premiun") ? "selected" : ""} ;>Premiun</option>
                                <option value="Cancelado" ${(cliente.status_Clientes == "Cancelado") ? "selected" : ""} ;>Cancelado</option>
                            </select>
                        </div>          
                        <div class="mb-3 col-lg-2">
                            <label for="DataNascimento" class="form-label">Data Nascimento:</label>
                            <input type="text" value="${cliente.dataNascimento}" class="form-control" name="DataNascimento" id="DataNascimento">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="Email" class="form-label">Email*:</label>
                            <input type="email" value="${cliente.email}" required class="form-control" name="Email" id="Email">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="Contato" class="form-label">Contato*:</label>
                            <input type="text" value="${cliente.contato}" required class="form-control"name="Contato" id="Contato">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="CEP" class="form-label">CEP:</label>
                            <input type="text" value="${cliente.CEP}" class="form-control"name="CEP" id="CEP">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="NumeroEnd" class="form-label">Numero:</label>
                            <input type="text" value="${cliente.numeroEnd}" class="form-control"name="NumeroEnd" id="NumeroEnd">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="ComplementoEnd" class="form-label">Complemento</label>
                            <input type="text" value="${cliente.complementoEnd}" class="form-control"name="ComplementoEnd" id="ComplementoEnd">
                        </div>

                        <button type="button" id="BntAlter" class="btn btn-primary">Submit</button>
                    </form>
                </c:if>
                <c:if test="${empty cliente}">
                    <form action="ClienteServlet"  method="POST" class="row">
                        <div class="mb-3 col-lg-4">
                            <label for="Nome" class="form-label">Nome*:</label>
                            <input type="text" class="form-control" required name="Nome">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="CPF" class="form-label">CPF*:</label>
                            <input type="text" class="form-control" required name="CPF" id="CPF">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="DataNascimento" class="form-label">Data Nascimento:</label>
                            <input type="text" class="form-control" name="DataNascimento" id="DataNascimento">
                        </div>
                        <div class="col-lg-3 mt-4">
                            <div class="form-check  form-check-inline">
                                <input type="checkbox" class="form-check-input" name="Feminino" id="Feminino">
                                <label class="form-check-label" for="exampleCheck1">Feminino</label>
                            </div>
                            <div class=" form-check  form-check-inline">
                                <input type="checkbox" class="form-check-input" name="Masculino" id="Masculino">
                                <label class="form-check-label" for="exampleCheck1">Masculino</label>
                            </div>
                        </div>
                        <div class="mb-3 col-lg-5">
                            <label for="Email" class="form-label">Email*:</label>
                            <input type="email" class="form-control" required name="Email" id="Email">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="Contato" class="form-label">Contato*:</label>
                            <input type="text" class="form-control" required name="Contato" id="CPF">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="CEP" class="form-label">CEP:</label>
                            <input type="text" class="form-control"name="CEP" id="CEP">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="NumeroEnd" class="form-label">Numero:</label>
                            <input type="text" class="form-control"name="NumeroEnd" id="NumeroEnd">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="ComplementoEnd" class="form-label">Complemento</label>
                            <input type="text" class="form-control"name="ComplementoEnd" id="ComplementoEnd">
                        </div>
                        <div class="mb-3 col-lg-4">
                            <label for="Status" class="form-label">Status</label>
                            <select class="form-select"  name="status_Clientes">
                                <option value="Ativo">Ativo</option>
                                <option value="Premiun">Premiun</option>
                                <option value="Cancelado">Cancelado</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>            
                </c:if>
            </div>
        </div>
    </body>
<c:import url="/Utils/Scripts.jsp"></c:import>

