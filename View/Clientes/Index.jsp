<!--NÃO MECHER ESSE É O BASE !! -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cliente</title>
        <c:import url="/Utils/Header.jsp"></c:import> 
    </head>

    <body>
        <div class="body">
            <c:import url="/Utils/Menu.jsp"></c:import> 
                <div class="container">
                    <h1>Consulta Clientes</h1>
                    <br/>
                    <button type="button" class="text-center btn btn-outline-dark mb-3" onclick="window.location.href = 'Cliente/CadCliente.jsp'"><span class="material-icons">person_add</span> Novo Cliente</button>

                    <table class="table">
                        <tr class="table-dark">
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>Email</th>
                            <th></th>
                            <th></th>
                        </tr>
                    <c:forEach var="clientes" items="${ListaClientes}">
                        <tr>
                            <td>${clientes.nome}</td>
                            <td>${clientes.cpf}</td>
                            <td>${clientes.email}</td>
                            <td><button type="button" class="btn btn-outline-dark" onclick="window.location.href='AlterarCliente?cpf=${clientes.cpf}'"><span class="material-icons">edit</span></button></td>
                            <td><button type="button" class="btn btn-outline-dark" onclick="ConfimarCliente(${clientes.cpf});"><span class="material-icons">delete</span></button></td>

                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </body>
    <c:import url="/Utils/Scripts.jsp"></c:import>
</html>

