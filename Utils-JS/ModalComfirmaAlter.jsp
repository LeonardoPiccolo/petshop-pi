<div class="modal" id="modalConfimaalter" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alerta</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="Close('modalConfimaalter')"></button>
      </div>
      <div class="modal-body">
          <p>Confirmar altera��o do cliente <span id="CPFCLIENTE"></span> ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="Close('modalConfimaalter')" data-bs-dismiss="modal">N�o</button>
        <button type="button" class="btn btn-primary" onclick="UpCliente();">Sim</button>
      </div>
    </div>
  </div>
</div>